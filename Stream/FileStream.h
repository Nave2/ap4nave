#pragma once
#include "OutStream.h"

class FileStream : public OutStream
{
public:
	FileStream(const char* filePath);
	~FileStream();
};