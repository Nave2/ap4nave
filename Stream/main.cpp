#include "OutStream.h"
#include "FileStream.h"
#include "OutStreamEncrypted.h"
#include "Logger.h"
#include <stdlib.h> // for the system("pause")

int get1500()
{
	return 1500;
}

int main(int argc, char **argv)
{
	OutStream out;
	out << "I am the Doctor and I'm ";
	out << get1500();
	out << " years old";
	endline();

	FileStream fout("outpit_file.txt");
	fout << "I am the Doctor and I'm ";
	fout << 1500;
	fout << " years old";
	fout << "\n"; // couldn't use endline() as explained in OutStream.cpp

	OutStreamEncrypted oute(1);
	oute << "I am the Doctor and I'm ";
	oute << get1500();
	oute << " years old";
	endline();

	Logger log1("log1_file.txt", true);
	Logger log2("log2_file.txt", true);
	log1.print("this is the 1st message, logger 1!\n");
	log2.print("this is the 2st message, logger 2!\n");
	log2.print("this is the 3st message, logger 2!\n");
	log1.print("this is the 4st message, logger 1!\n");

	system("pause");
	return 0;
}
