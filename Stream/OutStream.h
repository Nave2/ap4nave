#pragma once
#include <stdio.h>

class OutStream
	{
	protected:
		FILE* _fout;

	public:
		OutStream();
		~OutStream();

		OutStream& operator<<(const char* str);
		OutStream& operator<<(int num);
		OutStream& operator<<(void(*pf)());
	};

void endline();