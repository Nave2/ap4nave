#include "OutStreamEncrypted.h"
#pragma warning(disable : 4996)

OutStreamEncrypted::OutStreamEncrypted(int offset)
{
	this->_fout = stdout;
	this->_offset = offset;
}

int strlen(const char* str)
{
	int count = 0;
	while (str[count] != NULL)
		count++;
	return count;
}

OutStreamEncrypted& OutStreamEncrypted::operator<<(const char* str)
{
	char* encryptedStr = new char[strlen(str) + 1]{ 0 };
	for (int i = 0; str[i] != NULL; i++) {
		if (32 <= str[i] && str[i] <= 126) {
			encryptedStr[i] = str[i] + this->_offset;
			if (encryptedStr[i] > 126)
				encryptedStr[i] = 32 + encryptedStr[i] - 126 - 1;
		}
		else
			encryptedStr[i] = str[i];
	}
	printf("%s", encryptedStr);
	delete[] encryptedStr;
	return *this;
}

OutStreamEncrypted& OutStreamEncrypted::operator<<(int num)
{
	char str[12];
	//I guess that if fprintf and fprintf() are allowed, so is sprintf() (?)
	sprintf(str, "%d", num);
	this->operator<< (str);
	return *this;
}
