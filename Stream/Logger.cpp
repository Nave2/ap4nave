#include "Logger.h"

unsigned int Logger::_count = 0;

Logger::Logger(const char* filename, bool logToScreen) :
	_fs(FileStream(filename))
{
	if (logToScreen)
		this->_os = new OutStream();
	else
		this->_os = nullptr;
}

Logger::~Logger()
{
	if (this->_os != nullptr)
		delete this->_os;
}

void Logger::print(const char* msg)
{
	//print _count
	this->_fs.operator<<(this->_count) << ". ";
	if (this->_os != nullptr)
		this->_os->operator<<(this->_count) << ". ";
	//print msg
	this->_fs.operator<<(msg);
	if (this->_os != nullptr)
		this->_os->operator<<(msg);
	this->_count++;
}
