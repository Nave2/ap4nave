#pragma once
#include "OutStream.h"
#include "FileStream.h"

class Logger
{
private:
	//it seems that these are the only propertys I'm required to add
	//and I don't know if I'm allowed to add a bool flag which would be a bit more readable
	//however, adding a flag doesn't really matter as the class could be implemented without it
	FileStream _fs;
	OutStream* _os;
	static unsigned int _count;
public:
	Logger(const char *filename, bool logToScreen);
	~Logger();

	void print(const char *msg);
};
