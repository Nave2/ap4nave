#include "FileStream.h"
#pragma warning(disable : 4996)

FileStream::FileStream(const char* filePath)
{
	//this is to clean the file in case such file already exists
	this->_fout = fopen(filePath, "w");
	fclose(this->_fout);
	this->_fout = fopen(filePath, "a");
}

FileStream::~FileStream()
{
	fclose(this->_fout);
}
