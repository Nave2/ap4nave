#include "streamlib.h"
#include <stdio.h>

namespace msl
{
	OutStream::OutStream()
	{
		this->_fout = stdout;

	}

	OutStream::~OutStream()
	{
	}

	OutStream& OutStream::operator<<(const char* str)
	{
		fprintf(this->_fout, str);
		return *this;
	}

	OutStream& OutStream::operator<<(int num)
	{
		fprintf(this->_fout, "%d", num);
		return *this;
	}

	OutStream& OutStream::operator<<(void(*pf)())
	{
		pf();
		return *this;
	}

	//Shouldn't this be a class's method, so it will be possible to 
	//use this inside FileStream to print new line to file?
	//was I supposed to change the declaration/scope of the function?
	void endline()
	{
		printf("\n");
	}

}